/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SSd
 */
public class TestDeleteProduct {
      public static void main(String[] args) {
        
        Connection c = null;

        Database db = Database.getInstance();

        c = db.getConnection();
        
        try {
            String sql = "DELETE FROM product      WHERE id = ?";
             PreparedStatement stmt = c.prepareStatement(sql);

            stmt.setInt(1, 5);
            int row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
          } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        db.close();

    }
}
