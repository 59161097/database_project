/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;
import java.util.ArrayList;

/**
 *
 * @author SSd
 */
public class Receipt {

    private int id;
    private Date created;
    private User seller;
    private Customer customer;
    private ArrayList<ReceiptDetail> recepitDetail;

    public Receipt(int id, Date created, User seller, Customer customer) {
        this.id = id;
        this.created = created;
        this.seller = seller;
        this.customer = customer;
        recepitDetail = new ArrayList<>();
    }

    public Receipt(User seller, Customer customer) {
        this(-1, null, seller, customer);
    }
    
    
    public  void deleteReceiptDetail(int row){
        recepitDetail.remove(row);
    }
    
    

    public void addReceiptDetail(int id, Product product, int amount, double price) {
       
        for(int row =0; row<recepitDetail.size();row++){
            
            ReceiptDetail r = recepitDetail.get(row);
            if(r.getProduct().getId()==product.getId()){
                r.addAmount(amount);
                        return;
            }
           
        }
        recepitDetail.add(new ReceiptDetail(id, product, amount, price, this));

    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1, product, amount,product.getPrice() );
    }

    public double getTotal() {
        double total = 0;

        for (ReceiptDetail r : recepitDetail) {
            total = total + r.getTotal();
        }
        return total;
    }

    
    
     @Override
    public String toString() {
        String str = "Receipt{" + "id=" + id 
                + ", created=" + created 
                + ", seller=" + seller 
                + ", customer=" + customer 
                + ",total="+ this.getTotal()
                +  "}\n" ;
    
    for(ReceiptDetail r: recepitDetail){
        str = str + r.toString()+ "\n";
    }
    return str;
    
    
    }
   
}
