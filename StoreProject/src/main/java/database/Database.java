/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;
// singenton

import com.mycompany.storeproject.poc.TestSelectProduct;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SSd
 */
public class Database {
    private  static Database instance = new Database();
    private Connection c;
    private  Database(){      
    }
    public static Database getInstance(){
        
            String dbPath = "./db/store.db";

           try {
            if (instance.c == null || instance.c.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.c = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Connection accepted");
            }
            } catch (ClassNotFoundException ex) {
                System.out.println("Error: JDBC is not exist");

            } catch (SQLException ex) {
                System.out.println("Error: Database Cannot conection");
            }
        
        return instance;
    }
    
    public static  void  close(){
        try {
            if (instance.c != null && !instance.c.isClosed()) {
                instance.c.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.c = null;
    }
    public   Connection getConnection(){
        return instance.c;
    }
    
}
